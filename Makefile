DEPLOY_CMD=echo "TODO deploy"
INST_DIR=/tmp
include vars.mk

build/front/.tsc_passed: src/front/*.ts
	mkdir -p build/front
	cd src/front && tsc
	touch $@

build: build/front/.tsc_passed

dist: build/front/.tsc_passed
	mkdir -p dist/
	cp src/back/*.php dist/
	cp src/front/*.{html,css} dist/
	cp build/front/*.js dist/

install: build
	mkdir -p "$(INST_DIR)"
	cp dist/* "$(INST_DIR)"/

deploy: install
	$(DEPLOY_CMD)

distclean: clean
	rm -rf dist/
	rm -rf build/

clean:
	rm -f *~ src/*~ src/*/{*~,#*#,.#*.js}
	rm -f build/front/{*.js,.tsc_passed}
	test -d build/front && rmdir build/front || true
