class Position {
  employee_id : string = "-1";
  path        : string = ""
  name        : string = "";
  elems       : Array<HTMLElement> = [];

  constructor (path : string) {
    l.logd (path, 'path', path);
    this.path = path;

    service_query ("position_get_employee_id", { position_path: path }, (id)   => {
      l.logd (path, 'employee_id', id);

      this.employee_id = id;
      this.update_elems (true);
    });
    service_query ("position_get_name",        { position_path: path }, (name) => {
      l.logd (path, 'name', name);

      this.name = name;
      this.update_elems (false);
    });
  }

  create_elem () : HTMLElement {
    l.logd (this.path, 'elems');

    const div : HTMLElement = document.createElement ("DIV");
    div.classList.add ("position");
    div['position'] = this;

    const employee_holder : HTMLElement = dom_append_new_elem (div, "div", "employee-holder");
    const employee_x      : HTMLElement = dom_append_new_elem (div, /*employee_holder,*/ "span", "employee-remove", "×");
    employee_x.onclick = (_ev) => { this.remove_employee (); };

    dom_append_new_elem (div, "em", "position-name");

    this.elems.push (div);
    this.update_elems (true); /* will populate our elems */

    return div;
  }

  remove_employee () : void {
    org.lookup (this.employee_id, (employee) => {
      employee.position_paths.remove (this.path);
    });

    service_query ("position_remove_employee", { "position_path": this.path }, (_id) => {
      this.set_employee_id ("-1");
    });
  }

  set_employee_id (employee_id : string) : void {
    this.employee_id = employee_id;
    console.log ("TODO: actually need to update position's employee ID on server!");
    this.update_elems (true);
  }

  update_employee (pos_elem : HTMLElement, replace_employee : boolean) : void {
    const employee_holder : HTMLElement = pos_elem.querySelector (".employee-holder");

    org.lookup (this.employee_id, (employee) => {
      employee.position_paths.add (this.path);

      if (replace_employee) {
        l.logd_path_step (this.path, 'employee_replace_create_elem:employee', employee);

        const employee_elem = employee.create_elem ();
        l.logd_path_step (this.path, 'employee_replace_create_elem:employee', employee_elem);

        employee_holder.replaceChildren (employee_elem);
      }

      employee.update_elems ();
    });
  }

  update_elems (replace_employee : boolean = false) : void {
    l.logd_path_step (this.path, `update_elems(replace_employee = ${replace_employee})`);

    for (const elem of this.elems) {
      elem.setAttribute ("pos_path", this.path);
      dom_query_set_inner_text (elem, ".position-name", this.name);
      this.update_employee (elem, replace_employee);
    }
  }
}

class Employee {
  employee_id    : string = "-1";
  name           : string = "";
  position_paths : Set<string>;

  elems         : Array<HTMLElement> = [];

  constructor (id : string = "-1", parent : HTMLElement = null) {
    this.set_id (id);
    this.position_paths = new Set ();

    if (parent != null) {
      let div : HTMLElement = this.create_elem ();
      parent.appendChild (div);
    }
  }

  update_name (new_name : string) : void {
    if (new_name != this.name) {
      service_query ("employee_update_name", { id: this.employee_id, name: new_name }, (name) => {
        this.name = name;
        this.update_elems ();
      });
    }
  }

  set_id (id : string) : void {
    this.employee_id = id;

    if (id != "-1") {
      service_query ("employee_get_name", { id: id }, (name) => {
        this.name = name;
        this.update_elems ();
      });
    } else {
      this.update_elems ();
    }
  }

  create_elem () : HTMLElement {
    let div : HTMLElement = document.createElement ("DIV");
    div.classList.add ("position-employee");
    div['employee'] = this;

    if (this.employee_id == "-1") {
      div.appendChild (document.createTextNode ("(vacant)"));
    } else {
      const employee_name : HTMLElement = dom_append_new_elem (div, "a", "employee-name");
      const employee_id   : HTMLElement = dom_append_new_elem (div, "span", "employee-id");

      employee_name.onclick = (_ev) => {
        const new_name : string = window.prompt ("New name:", this.name);
        console.log (new_name);
        if (new_name) {
          this.update_name (new_name);
        }
      };
    }

    this.elems.push (div);
    return div;
  }

  update_elems () {
    for (let elem of this.elems) {
      elem.setAttribute ("employee_id", this.employee_id);
      if (this.employee_id != "-1") {
        dom_query_set_inner_text (elem, ".employee-name", (this.employee_id != "-1" ? this.name : "(Vacant)"));
        dom_query_set_inner_text (elem, ".employee-id",   (this.employee_id != "-1" ? this.employee_id : ""));
      }
    }
  }
}

class Organization {
  employees : Object = {}; /* id:string => Employee */
  vacant : Employee = new Employee ("-1");

  lookup (id : string, cb : Function = (employee) => {}) : void {
    if (id == "-1") {
      cb (this.vacant);
    }

    if (!this.employees[id]) {
      this.employees[id] = new Employee (id);
    }

    cb (this.employees[id]);
  }

  display_tree_position (pos_path : string, ul : HTMLElement) {
    const li       : HTMLElement = document.createElement ("LI");
    const position : Position    = new Position (pos_path);

    /* build subordinate list */
    const sub_ul : HTMLElement = document.createElement ("UL");
    sub_ul.classList.add ("subordinates");

    service_query ("position_get_subordinate_paths", { position_path: pos_path }, (sub_paths) => {
      sub_paths.map (sp => this.display_tree_position (sp, sub_ul));
    });

    li.appendChild (position.create_elem ());
    li.appendChild (sub_ul);
    ul.appendChild (li);
  }

  display_tree () : void {
    const root_ul : HTMLElement = document.querySelector ("DIV#organization-tree > ul");
    root_ul.replaceChildren ();
    this.display_tree_position ("0", root_ul);
  }

  display_list () : void {
    const ul : HTMLElement = document.querySelector ("DIV#employee-list > ul");
    ul.replaceChildren ();
    service_query ("list_employee_ids", {}, (ids) => {
      for (const id of ids) {
        const li : HTMLElement = dom_append_new_elem (ul, "li", "el-employee");
        li.setAttribute ("employee_id", id);

        org.lookup (id, (employee) => {
          li.appendChild (employee.create_elem ());
        });
      }
    });
  }
}
