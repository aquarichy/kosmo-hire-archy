function dom_query_set_inner_text (origin : HTMLElement, selector : string, inner_text : string) {
  try {
    (origin.querySelector (selector) as HTMLElement).innerText = inner_text;
  } catch (e) {
    console.log (`ERROR: dom_query_set_inner_text: selector '${selector}'`);
    throw e;
  }
}

function dom_append_new_elem (parent : HTMLElement, elem_name : string, class_name : string = null, inner_text : string = null) : HTMLElement {
  const elem : HTMLElement = document.createElement (elem_name);
  if (class_name) {
    elem.classList.add (class_name);
  }
  elem.innerText = inner_text ?? "";
  parent.appendChild (elem);
  return elem;
}
