function service_query_xhr_loaded (xhr : XMLHttpRequest, cb : Function) : void {
  switch (xhr.status) {
    case 200:
      try {
        let obj = JSON.parse (xhr.response)
        cb (obj);
      } catch (e) {
        console.log (`Response contained invalid JSON!`);
        console.log (e);
        console.log (xhr);
      }
      break;

    case 500:
    default:
      console.log (`service_query: status ${xhr.status}`);
      throw `service_query: status ${xhr.status}`;
  }
}

function service_query (action : string, params : Object, cb : Function) : void {
  const xhr : XMLHttpRequest  = new XMLHttpRequest ();
  xhr.addEventListener ("load", (ev) => {
    service_query_xhr_loaded (xhr, cb);
  });
  /* TODO: onerror handling */

  const param_str : string = Object.keys (params ?? {}).map (x => `${x}=${params[x]}`).join ("&");

  xhr.open ("GET", `service.php?action=${action}&${param_str}`);
  xhr.send ();
}
