class Logger {
  messages : Array<string> = [];
  console_level : number= 1;  /* 0: nothing, 1: errors, 2: + warnings, 3: + messages, 4: + debug */

  constructor (level : number = 4) {
    this.console_level = level;
  }

  _log (level : number, ...args) : void {
    const dt = (new Date ()).toISOString ();

    if (level <= this.console_level) {
      console.log (dt, ...args);
    }

    for (const arg of args) {
      this.messages.push (`kha:${dt}:${level}: ${arg}`);
      if (typeof (arg) != "string") {
        this.messages.push (arg);
      }
    }
  }

  loge (...args) : void {
    this._log (1, ...args);
  }
  logw (...args) : void {
    this._log (2, ...args);
  }
  log (...args) : void {
    this._log (3, ...args);
  }
  logd (...args) : void {
    this._log (4, ...args);
  }
  logq (...args) : void {
    this._log (999, ...args); /* quiet logging */
  }

  logd_path_step (path : string, field : string, obj = null) {
    this.logd (`position [${path}] @ [${field}]`, obj)
  }
}
