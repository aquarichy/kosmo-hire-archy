<?php
require_once ("organization.php");

function generate_full_name () {
  return generate_name ()." ".generate_name ();
}

function generate_name () {
  $consonants = [ "b", "br", "bl",
                  "c", "cr", "cl", "ch",
                  "d", "dr", "dw",
                  "f", "fr", "fl",
                  "g", "gr", "gl", "gh", "gw",
                  "h",
                  "j",
                  "k", "kr", "kl",
                  "l",
                  "m",
                  "n",
                  "p", "pr", "pl", "ph",
                  "q",
                  "r", "rh",
                  "s", "sl", "sh",
                  "t", "tr", "th",
                  "v",
                  "w", "wr",
                  "x",
                  "y",
                  "z" ];
  $consonants_len = count ($consonants);
  $vowels = [ "a", "ai", "o", "u", "i", "ie", "e" ];
  $vowels_len = count ($vowels);

  $name = "";
  $type = rand (0, 1); # 0: vowel, 1: consonant
  $vowel_cnt = 0;
  while ((rand (0, 3) < 3 || strlen ($name) < 2 || $vowel_cnt == 0) && strlen ($name) < 12) {
    if ($type == 0) {
      $name .= $vowels [rand (0, $vowels_len - 1)];
      $vowel_cnt++;
      $type = 1;
    } else {
      $name .= $consonants [rand (0, $consonants_len - 1)];
      $type = 0;
    }
  }

  return ucfirst ($name);
}

srand (17);

$g_employee_id = 120;
$g_employees = [ "101" => new Employee (generate_full_name ()) ]; # "120" => new Employee (...)
$g_max_level = 4;
$g_max_subordinates = 8;

function generate_org_tree ($boss_id, $level) {
  global $g_employee_id;
  global $g_employees;
  global $g_max_level;
  global $g_max_subordinates;

  $subordinates = [];

  if ($level < $g_max_level) {
    for ($i = 0; $i < rand (0, $g_max_subordinates); $i++) {
      $employee = new Employee (generate_full_name ());
      $employee_id = $g_employee_id;
      $g_employee_id++;

      $g_employees["{$employee_id}"] = $employee;
      # print_r ("<pre>".print_r ($g_employees, true)."</pre>");

      array_push ($subordinates, generate_org_tree ($employee_id, $level + 1));
    }
  }

  return new Position ($boss_id, $subordinates);
}

# print_r ("<pre>".print_r (generate_org_tree (101, 0), true)."</pre>");

$g_tree = generate_org_tree (101, 0);

$org = new Organization (
  # position_levels
  [
    "Director",
    "Senior Manager",
    "Manager",
    "Senior Developer",
    "Developer",
  ],
  $g_employees,
  new Node ([$g_tree])
);

# error_log (print_r ($org->employees, true));

$org_old = new Organization (
  # position_levels
  [
    "Director",
    "Senior Manager",
    "Manager",
    "Senior Developer",
    "Developer",
  ],
  # employees
  [
    # Employee Number => Employee Data
    101 => new Employee ("Af Al"),
    102 => new Employee ("Baf Bal"),
    103 => new Employee ("Caf Cal"),
    104 => new Employee ("Daf Dal"),
    105 => new Employee ("Ef El"),
    106 => new Employee ("Fef Fel"),
    107 => new Employee ("Gef Gel"),
  ],
  new Node ([
    # director
    new Position (101, [
      # sr mgrs
      new Position (102, [
        # mgrs
        new Position (104, [
          # sr devs
          new Position (106, [

          ]),
          new Position (107, []),
        ]),
        new Position (-1, []),
      ]),
      new Position (105, []),
    ]),
  ])
);
?>
