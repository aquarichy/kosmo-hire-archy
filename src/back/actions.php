<?php
require_once ("util.php");
require_once ("data.php");

function position_for_path ($path) {
  global $org;

  $parts = explode (",", $path);
  $history = [];
  $cur_pos = $org->position_tree;

  foreach ($parts as $pos_i) {
    array_push ($history, $pos_i);
    $cur_pos = dict_get ($cur_pos->children, $pos_i, "Position path '".implode (",", $history)."' is invalid");
  }

  return $cur_pos;
}

function param_get_position_path ($action) {
  return param_get ("position_path", "'{$action}' requires 'position_path'");
}

function param_get_employee_id ($action) {
  return param_get ("id", "'{$action} requires employee 'id' parameter");
}

function json_print ($obj, $err_msg = null) {
  print (json_encode_checked ($obj, $err_msg ?? "Failed to encode object as JSON: ".print_r ($obj, true)));
}

$actions = [
  "position_get_subordinate_paths" => function ($action, $_org) {
    $position_path = param_get_position_path ($action);
    $pos = position_for_path ($position_path);
    $sub_paths = [];
    foreach ($pos->children as $i => $subordinate) {
      array_push ($sub_paths, "{$position_path},{$i}");
    }

    json_print ($sub_paths, "Failed to encode subordinate paths '".print_r ($sub_paths, TRUE)."'");
  },

  "position_get_name" => function ($action, $org) {
    $position_path = param_get_position_path ($action);
    $depth = count (explode (",", $position_path)) - 1;
    $position_name = dict_get ($org->position_levels, $depth, "No position level defined for depth '{$depth}'");

    json_print ($position_name, "Failed to encode position name '{$position_name}'");
  },

  "position_get_employee_id" => function ($action, $_org) {
    $position_path = param_get_position_path ($action);
    $pos = position_for_path ($position_path);

    json_print ($pos->employee_id, "Failed to encode position's employee ID '{$pos->employee_id}'");
  },

  "list_employee_ids" => function ($action, $org) {
    $ids = [];
    foreach (array_keys ($org->employees) as $employee_id) {
      array_push ($ids, $employee_id);
    }

    json_print ($ids, "Failed to encode employee IDs '".print_r ($ids, TRUE)."'");
  },

  "employee_get_name" => function ($action, $org) {
    $id = param_get_employee_id ($action);
    $employee = dict_get ($org->employees, $id, "No employee found for id '{$id}'");

    json_print ($employee->name, "Failed to encode employee name '{$employee->name}'");
  },

  "employee_update_name" => function ($action, $org) {
    $id = param_get_employee_id ($action);
    $employee = dict_get ($org->employees, $id, "No employee found for id '{$id}'");
    $new_name = param_get ("name", "{$action} requires parameter 'name'");
    $employee->name = $new_name;

    json_print ($new_name, "Failed to encode employee's new name '{$new_name}'");
  },

  "position_remove_employee" => function ($action, $org) {
    $position_path = param_get_position_path ($action);
    $position = position_for_path ($position_path);

    $id = $position->employee_id;
    $employee = dict_get ($org->employees, $id, "No employee found for id '{$id}'"); # make sure it's there
    $employee->unhired = true;

    $position->employee_id = -1;

    json_print (-1);
  },
];
?>
