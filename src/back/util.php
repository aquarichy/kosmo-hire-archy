<?php
function param_get ($key, $err_msg, $status_code = 400) {
  if (! isset ($_GET[$key])) {
    throw new HttpException ("GET param '{$key}' not set.  {$err_msg}", $status_code);
  }
  return $_GET[$key];
}

function dict_get ($dict, $key, $err_msg, $status_code = 404) {
  if (! isset ($dict[$key])) {
    throw new HttpException ("Key '{$key}' not set.  {$err_msg}", $status_code);
  }
  return $dict[$key];
}

function json_encode_checked ($obj, $err_msg) {
  $json = json_encode ($obj);

  if ($json == false) {
    throw new Exception ("ERROR: Failed to encode as JSON.  {$err_msg}");
  }

  return $json;
}
?>
