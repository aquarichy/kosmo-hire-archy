<?php
declare(strict_types=1);

class HttpException extends Exception {
  public int $status_code = 500;

  function __construct (string $msg, int $status_code) {
    parent::__construct($msg);
    $this->status_code = $status_code;
  }

  function getStatusCode () {
    return $this->status_code;
  }
}

require_once ("util.php");
require_once ("organization.php"); # classes for Organization, Position<Node>, Node, and Employee
require_once ("data.php");         # pull in test data $org
require_once ("actions.php");

function dbg_get_post () {
  global $org;

  if (($_GET['dbg'] ?? "0") == "1") {
    print ("<pre>");

    print ("<hr />GET\n");
    print_r ($_GET);

    print ("<hr />POST\n");
    print_r ($_POST);

    print ("<hr />org root\n");
    print_r ($org);

    print ("</pre>");
  }
}

function parse_request () {
  global $actions;
  global $org;

  if (count ($_GET) > 0) {
    try {
      $action = param_get ("action", "Missing GET parameter.");
      $action_func = dict_get ($actions, $action, "Unknown 'action'", 400);
      $action_func ($action, $org);
    } catch (HttpException $e) {
      error_log ($e->getMessage ());
      http_response_code ($e->getStatusCode ());
    } catch (Exception $e) {
      error_log ($e->getMessage ());
      http_response_code (500);
    }
  } else if (count ($_POST) > 0) {
    # ...
  } else {
    # ...
  }

  dbg_get_post ();
}

parse_request ();
?>

