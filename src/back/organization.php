<?php
class Node {
  public array $children;

  function __construct (array $children = []) {
    $this->children = $children;
  }
}

class Position extends Node {
  public int $employee_id = -1;  # -1 = vacant

  function __construct (int $employee_id = -1, array $subordinates = []) {
    $this->employee_id = $employee_id;
    $this->children = $subordinates;
  }
}

class Employee {
  public string $name = "";

  function __construct (string $name) {
    $this->name = $name;
  }
}

class Organization {
  public array $position_levels; # array of string, index corresponds to position level
  public array $employees; # dict of: employee_id:int => Employee
  public Node $position_tree; # root Node for a tree, with descendents that are all Position

  function __construct (array $position_levels, array $employees, Node $position_tree) {
    $this->position_levels = $position_levels;
    $this->employees = $employees;
    $this->position_tree = $position_tree;
  }
}
?>
